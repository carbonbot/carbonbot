#!/usr/bin/env python3
import logging
import os
import sys
from siliconbot import Silicon
from siliconbot import plugin_loader
from siliconbot.adapter.console import ConsoleAdapter
from siliconbot.adapter.irc import IRCAdapter
from siliconbot.adapter.matrix import MatrixAdapter
from siliconbot.adapter.telegram import TelegramAdapter


def str_to_bool(s):
    s = s.lower()
    return s and s != "0" and not s.startswith("f") and not s.startswith("n")


def main(*args, **kwargs):
    console = os.environ.get('SILICON_CONSOLE') or "--console" in args
    telegram = os.environ.get('SILICON_TELEGRAM') or "--telegram" in args
    irc = os.environ.get('SILICON_IRC') or "--irc" in args
    matrix = os.environ.get('SILICON_MATRIX') or "--matrix" in args

    adapters = {}

    if console:
        adapters["console"] = ConsoleAdapter()
    else:
        if telegram:
            adapters["telegram_silicon_bot"] = \
                TelegramAdapter(os.environ.get('TELEGRAM_BOT_TOKEN'),
                                os.environ.get('TELEGRAM_BOT_OWNER'),
                                os.environ.get('TELEGRAM_BOT_OWNER_ID'),
                                identifier=os.environ.get('TELEGRAM_IDENTIFIER', default='[!/]')
                                )

        if irc:
            adapters["irc_silicon_bot"] = \
                IRCAdapter(
                    os.environ.get('IRC_SERVER_ADDRESS'),
                    int(os.environ.get('IRC_SERVER_PORT')),
                    str_to_bool(os.environ.get('IRC_SERVER_IS_SSL')),
                    os.environ.get('IRC_CHANNELS').split(","),
                    os.environ.get('IRC_OWNER'),
                    nick=os.environ.get('IRC_NICK'),
                    is_sasl=str_to_bool(os.environ.get('IRC_SERVER_IS_SASL')),
                    password=os.environ.get('IRC_SASL_PASSWORD') \
                        if os.environ.get('IRC_SERVER_IS_SASL') else None,
                    identifier=os.environ.get('IRC_IDENTIFIER', default='!')
                )

        if matrix:
            adapters["matrix_silicon_bot"] = \
                MatrixAdapter(os.environ.get('MATRIX_SERVER'),
                              os.environ.get('MATRIX_USERNAME'),
                              os.environ.get('MATRIX_PASSWORD'),
                              os.environ.get('MATRIX_OWNER'),
                              device_id=os.environ.get('MATRIX_DEVICE_ID'),
                              rooms=None,
                              identifier=os.environ.get('MATRIX_IDENTIFIER', default=r'\$'))

    silicon = Silicon(adapters,
                    source_url=os.environ.get('SOURCE_URL'),
                    privacy_policy_url=os.environ.get('PRIVACY_POLICY_URL'))

    plugin_loader.load_and_register_all(silicon)

    if not console:
        # Logging code.
        # TODO Clean up
        logging.basicConfig(
            level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    silicon.run()

if __name__ == '__main__':
    main(*sys.argv)
