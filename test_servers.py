import requests

searxes = ["https://search.modalogi.com/", "https://searx.openpandora.org/",   "https://search.paulla.asso.fr/",
           "https://search.seds.nl/",      "https://searx.win/",               "https://search.activemail.de/",
           "https://searx.site/",          "https://searx.at/",                "https://search.jpope.org/",
           "https://searx.aquilenet.fr/",  "https://searx.angristan.xyz/",     "https://s3arch.eu/",
           "https://searx.haxors.club/",   "https://searx.ru/",                "https://searx.pwoss.xyz/",
           "https://openworlds.info/",     "https://searx.zareldyn.net/",      "https://search.biboumail.fr/",
           "https://finden.tk/",           "https://searx.abenthung.it/",      "https://searx.nogafa.org/",
           "https://searx.dk/",            "https://searx.gotrust.de/",        "https://search.gibberfish.org/",
           "https://searx.prvcy.eu/",      "https://suche.elaon.de/",          "https://suche.ftp.sh/",
           "https://searx.cc/",            "https://searx.ch/",                "https://searx.nulltime.net/",
           "https://searx.me/",            "https://www.searx.de/",            "https://searx.laquadrature.net/",
           "https://searx.oe5tpo.com/",    "https://search.homecomputing.fr/", "https://searx.potato.hu/",
           "https://framabee.org/",        "https://searx.libmail.eu/",        "https://search.blackit.de/",
           "https://search.koehn.com/",    "https://searx.drakonix.net/",      "https://search.4ray.co/",
           "https://search.azkware.net/",  "https://search.alecpap.com/",      "https://search.st8.at/",
           "https://search.paviro.de/",    "https://searx.infini.fr/",         "https://searx.kvch.me/",
           "https://searx.fr32k.de/",      "https://searx.remote-shell.net/",  "https://wtf.roflcopter.fr/searx",
           "https://searchx.mobi/",        "https://searx.antirep.net/",       "https://search.stinpriza.org/",
           "https://search.mdosch.de/",    "https://www.searxs.eu/",           "https://searx.foo.li/",
           "https://searx.pofilo.fr/",     "https://searx.openhoofd.nl/",      "https://searx.ahh.si/",
           "https://searx.org/",           "https://search.spaeth.me/",        "https://p9e.de/",
           "https://s.bacafe.xyz/",        "https://searx.tyil.nl/",           "https://seeks.hsbp.org/",
           "https://icebal.com/",          "https://www.opengo.nl/",           "https://search.moravit.com/",
           "https://beezboo.com/",         "https://searx.itunix.eu/",         "https://search.snopyta.com/",
           "https://searx.solusar.de/"]

params = {}
params['q'] = 'searx'
params['format'] = 'json'

for server in searxes:
    print("Testing "+server)
    try:
        response = requests.get("{}?".format(server if server[-1]=='/' else server+"/"), params=params, headers = {'User-agent': 'SiliconBot 2.0'})
        results = response.json()
    except:
        print(response)
        print(response.text)
