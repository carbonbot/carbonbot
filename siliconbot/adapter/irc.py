import base64
from datetime import datetime
import logging
import re
import socket
import ssl
import threading
import time
import queue
from siliconbot.adapter import Adapter
from siliconbot.constants import SOFTWARE_NAME, VERSION, DESCRIPTION_AND_AUTHORS


# {{{
# Error replies
ERR_NOSUCHNICK       = "401"
ERR_NOSUCHSERVER     = "402"
ERR_NOSUCHCHANNEL    = "403"
ERR_CANNOTSENDTOCHAN = "404"
ERR_TOOMANYCHANNELS  = "405"
ERR_WASNOSUCHNICK    = "406"
ERR_TOOMANYTARGETS   = "407"
ERR_NOORIGIN         = "409"
ERR_NORECIPIENT      = "411"
ERR_NOTEXTTOSEND     = "412"
ERR_NOTOPLEVEL       = "413"
ERR_WILDTOPLEVEL     = "414"
ERR_UNKNOWNCOMMAND   = "421"
ERR_NOMOTD           = "422"
ERR_NOADMININFO      = "423"
ERR_FILEERROR        = "424"
ERR_NONICKNAMEGIVEN  = "431"
ERR_ERRONEUSNICKNAME = "432"
ERR_NICKNAMEINUSE    = "433"
ERR_NICKCOLLISION    = "436"
ERR_USERNOTINCHANNEL = "441"
ERR_NOTONCHANNEL     = "442"
ERR_USERONCHANNEL    = "443"
ERR_NOLOGIN          = "444"
ERR_SUMMONDISABLED   = "445"
ERR_USERSDISABLED    = "446"
ERR_NOTREGISTERED    = "451"
ERR_NEEDMOREPARAMS   = "461"
ERR_ALREADYREGISTRED = "462"
ERR_NOPERMFORHOST    = "463"
ERR_PASSWDMISMATCH   = "464"
ERR_YOUREBANNEDCREEP = "465"
ERR_KEYSET           = "467"
ERR_CHANNELISFULL    = "471"
ERR_UNKNOWNMODE      = "472"
ERR_INVITEONLYCHAN   = "473"
ERR_BANNEDFROMCHAN   = "474"
ERR_BADCHANNELKEY    = "475"
ERR_NOPRIVILEGES     = "481"
ERR_CHANOPRIVSNEEDED = "482"
ERR_CANTKILLSERVER   = "483"
ERR_NOOPERHOST       = "491"
ERR_UMODEUNKNOWNFLAG = "501"
ERR_USERSDONTMATCH   = "502"

# Command responses
RPL_NONE             = "300"
RPL_USERHOST         = "302"
RPL_ISON             = "303"
RPL_AWAY             = "301"
RPL_UNAWAY           = "305"
RPL_NOWAWAY          = "306"
RPL_WHOISUSER        = "311"
RPL_WHOISSERVER      = "312"
RPL_WHOISOPERATOR    = "313"
RPL_WHOISIDLE        = "317"
RPL_ENDOFWHOIS       = "318"
RPL_WHOISCHANNELS    = "319"
RPL_WHOWASUSER       = "314"
RPL_ENDOFWHOWAS      = "369"
RPL_LISTSTART        = "321"
RPL_LIST             = "322"
RPL_LISTEND          = "323"
RPL_CHANNELMODEIS    = "324"
RPL_NOTOPIC          = "331"
RPL_TOPIC            = "332"
RPL_INVITING         = "341"
RPL_SUMMONING        = "342"
RPL_VERSION          = "351"
RPL_WHOREPLY         = "352"
RPL_ENDOFWHO         = "315"
RPL_NAMREPLY         = "353"
RPL_ENDOFNAMES       = "366"
RPL_LINKS            = "364"
RPL_ENDOFLINKS       = "365"
RPL_BANLIST          = "367"
RPL_ENDOFBANLIST     = "368"
RPL_INFO             = "371"
RPL_ENDOFINFO        = "374"
RPL_MOTDSTART        = "375"
RPL_MOTD             = "372"
RPL_ENDOFMOTD        = "376"
RPL_YOUREOPER        = "381"
RPL_REHASHING        = "382"
RPL_TIME             = "391"
RPL_USERSSTART       = "392"
RPL_USERS            = "393"
RPL_ENDOFUSERS       = "394"
RPL_NOUSERS          = "395"
RPL_TRACELINK        = "200"
RPL_TRACECONNECTING  = "201"
RPL_TRACEHANDSHAKE   = "202"
RPL_TRACEUNKNOWN     = "203"
RPL_TRACEOPERATOR    = "204"
RPL_TRACEUSER        = "205"
RPL_TRACESERVER      = "206"
RPL_TRACENEWTYPE     = "208"
RPL_TRACELOG         = "261"
RPL_STATSLINKINFO    = "211"
RPL_STATSCOMMANDS    = "212"
RPL_STATSCLINE       = "213"
RPL_STATSNLINE       = "214"
RPL_STATSILINE       = "215"
RPL_STATSKLINE       = "216"
RPL_STATSYLINE       = "218"
RPL_ENDOFSTATS       = "219"
RPL_STATSLLINE       = "241"
RPL_STATSUPTIME      = "242"
RPL_STATSOLINE       = "243"
RPL_STATSHLINE       = "244"
RPL_UMODEIS          = "221"
RPL_LUSERCLIENT      = "251"
RPL_LUSEROP          = "252"
RPL_LUSERUNKNOWN     = "253"
RPL_LUSERCHANNELS    = "254"
RPL_LUSERME          = "255"
RPL_ADMINME          = "256"
RPL_ADMINLOC1        = "257"
RPL_ADMINLOC2        = "258"
RPL_ADMINEMAIL       = "259"

# Reserved numerics
RPL_TRACECLASS       = "209"
RPL_STATSQLINE       = "217"
RPL_SERVICEINFO      = "231"
RPL_ENDOFSERVICES    = "232"
RPL_SERVICE          = "233"
RPL_SERVLIST         = "234"
RPL_SERVLISTEND      = "235"
RPL_WHOISCHANOP      = "316"
RPL_KILLDONE         = "361"
RPL_CLOSING          = "362"
RPL_CLOSEEND         = "363"
RPL_INFOSTART        = "373"
RPL_MYPORTIS         = "384"
ERR_YOUWILLBEBANNED  = "466"
ERR_BADCHANMASK      = "476"
ERR_NOSERVICEHOST    = "492"

# IRCv3 codes
RPL_ISUPPORT         = "005"
RPL_STARTTLS         = "670"
ERR_STARTTLS         = "691"
RPL_MONONLINE        = "730"
RPL_MONOFFLINE       = "731"
RPL_MONLIST          = "732"
RPL_ENDOFMONLIST     = "733"
ERR_MONLISTFULL      = "734"
RPL_LOGGEDIN         = "900"
RPL_LOGGEDOUT        = "901"
ERR_NICKLOCKED       = "902"
RPL_SASLSUCCESS      = "903"
ERR_SASLFAIL         = "904"
ERR_SASLTOOLONG      = "905"
ERR_SASLABORTED      = "906"
ERR_SASLALREADY      = "907"
RPL_SASLMECHS        = "908"
# }}}


class IRCAdapter(Adapter):
    def __init__(self, server, port, is_ssl, channels, owner, nick="Silicon", codec="UTF-8", is_sasl=False, password=None, identifier="!", nick_as_identifier=True):
        super(IRCAdapter, self).__init__(identifier=identifier)
        self.server = server
        self.port = port
        self.is_ssl = is_ssl
        self.is_sasl = is_sasl
        self.orig_nick = nick
        self.nick = nick
        self.nick_as_identifier = nick_as_identifier
        self.channels = channels
        self.owner = owner
        self.codec = codec
        self.password = password

        self.PRIVMSG_re = re.compile(
            r':(?P<nick>[^\s]+)![^\s]+@[^\s]+ PRIVMSG (?P<chan>[^\s]+) :(?P<msg>.*)')

        threading.Thread.__init__(self)
        self.logger = logging.getLogger("IRCAdapter")
        logging.basicConfig(level=logging.DEBUG)

    def ping(self, msg):
        pong = msg.lstrip("PING :")
        self.raw_send("PONG :{}\n".format(pong))

    def register_callback(self, func, _id):
        self.execute = func
        self._id = _id

    def raw_send(self, message):
        self.sock.send(message.encode(self.codec))
        self.logger.info("→ {}".format(message.rstrip()))

    def send(self, message, to, command="NOTICE"):
        if not message.strip("\r\n").strip():
            return
        for line in message.split("\n"):
            message = line.strip(" ")
            self.raw_send(command + " " + to + " :" + message + "\n")

    def reply(self, message, to, group):
        return self.send(to+": "+message, group)

    def join_channel(self, ch):
        self.raw_send("JOIN %s\n" % ch)

    def get_ident(self, pm):
        nick_regex = re.escape(self.nick) + "[,:] "

        possibilities = []

        if self.identifier:
            possibilities.append(self.identifier)

        if self.nick_as_identifier or not self.identifier:
            possibilities.append(nick_regex)

        if pm:
            possibilities.append("")

        return "(?:{})".format("|".join(possibilities))

    def handle_message(self, chan, user, message):
        is_pm = chan == user

        if is_pm and message.upper() == "\x01VERSION\x01":
            self.logger.info("{} queried our version".format(user))
            self.send("\x01VERSION {}:{}:\x01".format(SOFTWARE_NAME, VERSION), chan, command="NOTICE")
            return

        if not is_pm:
            # Check if the message was sent through a bridging bot

            # First we check the sender's nick
            m_user = re.fullmatch(r'[^a-zA-Z]+|apiaceae|xxxx_', user)
            if m_user:

                # Then we check the message. Also support color codes
                m_message = re.fullmatch(r'(?:\x03[0-9]{2}|[ \x01-\x0f])*<([^>]+)>[ \x01-\x0f]*:[ \x01-\x0f]*(.+)', message)
                #                                                         ↑         ↑
                #                                                         |         `- blanket match IRC formatting reset codes
                #                                                         `- user name. formatting codes are stripped out later

                if m_message:
                    # Move information so that we think this message came from that user
                    # Append [proxy] to avoid impersonation
                    bridge_nick = user

                    # Remove formatting codes and zero-width spaces (U+200B) from the user name part
                    bridged_user = re.sub(r'\x03[0-9]{2}|[\x01-\x0f]|​', "", m_message[1])

                    if bridge_nick == "apiaceae":
                        user = "@{} [{}]".format(bridged_user, bridge_nick)
                    else:
                        user = "{}[{}]".format(bridged_user, bridge_nick)
                    message = m_message[2]

            self.logger.info("Received message in ``{}'' from ``{}'': ``{}''".format(chan, user, message))

        else:
            self.logger.info("Received PM from ``{}'': ``{}''".format(user, message))

        metadata = {"from_user": user, "from_group": chan, "when": datetime.now(),
                    "_id": self._id, "ident": self.get_ident(is_pm), "type": self.__class__.__name__,
                    # FIXME: Check for other admins in the channel.
                    "mentioned": self.nick in message, "is_mod": user == self.owner,
                    "message_id": user, "pm": is_pm}
        self.execute(message, metadata)

    def _recv_next_line(self):
        next_line = None

        no_recv_amount_threshold = 100000
        no_recv_time_treshold = 2.0

        i = 0
        no_recv_start = time.time()
        while next_line is None:
            try:
                next_line = self.recv_queue.get_nowait()
            except queue.Empty:
                incoming = self.sock.recv(2048).decode(self.codec)

                got_new = False
                for line in incoming.split("\n"):
                    line = line.strip()
                    if line:
                        self.recv_queue.put(line)
                        got_new = True

                if not got_new:
                    # Safety net to avoid looping
                    i += 1
                    if i >= no_recv_amount_threshold and time.time() - no_recv_start < no_recv_time_treshold:
                        self.logger.error("Receiving loop has spun %d times in under %f seconds without receiving content, aborting adapter", i, no_recv_time_treshold)
                        return None

        self.logger.info("← {}".format(next_line))
        return next_line

    def run(self):
        raw_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        raw_socket.connect((self.server, self.port))  # Create & connect socket

        self.recv_queue = queue.Queue()

        try:
            if self.is_ssl:
                # Optionally wrap socket with SSL
                self.sock = ssl.wrap_socket(raw_socket)
            else:
                self.sock = raw_socket

            authenticated = False
            if self.is_sasl:
                # SASL request has to be sent before everything else.
                self.raw_send("CAP REQ :sasl\n")

            self.raw_send("NICK %s\n" % self.nick)
            self.raw_send("USER {0} {0} {0} :{1}, {2}\n".format(self.nick, SOFTWARE_NAME, DESCRIPTION_AND_AUTHORS))

            self.logger.info("Logging in...")

            # Client Capability Negotiation loop
            while True:
                msg = self._recv_next_line()

                # None signals that the receiving loop spinned to fast, and we should abort
                if msg is None:
                    return

                # Remove nick from msg to avoid conflicts with IRC keywords
                msg = msg.replace(self.nick, "")

                if "MODE %s" % self.nick in msg or "MOTD" in msg:
                    self.logger.info("Okay, server doesn't want to do capability negotiation, continuing")
                    break

                elif msg.startswith("PING :"):
                    self.ping(msg)

                # Nickname already in use
                elif " " + ERR_NICKNAMEINUSE + " " in msg:
                    self.nick += "_"
                    self.raw_send("NICK %s\n" % self.nick)

                elif self.is_sasl and "CAP " in msg and " ACK :sasl" in msg:
                    self.raw_send("AUTHENTICATE PLAIN\n")

                elif self.is_sasl and "AUTHENTICATE" in msg:
                    auth = ('{sasl_username}\0'
                            '{sasl_username}\0'
                            '{sasl_password}').format(sasl_username=self.orig_nick, sasl_password=self.password)
                    auth = base64.encodestring(auth.encode(self.codec))
                    auth = auth.decode(self.codec).rstrip('\n')
                    self.raw_send("AUTHENTICATE " + auth + "\n")

                # Auth succeeded; end Client Capability Negotiation phase
                elif self.is_sasl and " " + RPL_SASLSUCCESS + " " in msg:
                    authenticated = True
                    self.logger.info("Authentication confirmed by server, exiting CAP loop")
                    self.raw_send("CAP END\n")
                    break

                elif self.is_sasl and (" " + ERR_SASLFAIL + " " in msg or " " + ERR_NICKLOCKED + " " in msg or " " + ERR_SASLTOOLONG + " " in msg):
                    self.logger.warning("Authentication failed, shutting down IRC adapter")
                    return

                elif self.is_sasl and (" " + ERR_SASLTOOLONG + " " in msg):
                    self.logger.warning("Authentication failed, server replied ERR_SASLTOOLONG, shutting down IRC adapter")
                    return

                elif "NOTICE " in msg or re.search(r"^:[^ ]* 900", msg):
                    pass

                elif re.search(r"^:[^ ]* 00[1-5]", msg):
                    self.logger.info("Server sends replies in range 1 through 5, so it considers the CAP loop done")
                    break

                else:
                    self.logger.info("Unexpected reply, ignoring")

            if self.is_sasl:
                if not authenticated:
                    self.logger.error("Authentication has not completed successfully! Aborting adapter")
                    return

            self.logger.info("Joining channels")
            for ch in self.channels:
                self.join_channel(ch)

            self.logger.info("Connection set up and CAP done")

            self._on_initialized(self.owner)

            while True:
                try:
                    msg = self._recv_next_line()

                    # None signals that the receiving loop spinned to fast, and we should abort
                    if msg is None:
                        return

                    if msg.startswith("PING :"):
                        self.ping(msg)
                        continue

                    m = self.PRIVMSG_re.fullmatch(msg)
                    if not m:
                        continue

                    sender = m.group("nick")
                    chan = m.group("chan")

                    # Handle PMs: set the channel name to the name of the sender
                    # This will make sure replies are sent to the correct place
                    if chan == self.nick:
                        chan = sender

                    self.handle_message(chan, sender, m.group("msg"))

                except Exception:
                    self.logger.error("Error while reading socket.", exc_info=True)

        finally:
            try:
                raw_socket.close()
            except Exception:
                self.logger.warning("Failed to close socket", exc_info=True)

# vim: set foldmethod=marker :
