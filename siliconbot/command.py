import traceback


class Command:
    def __init__(self, regex, title, description, on_exec, raw_match=False,
                 display_condition=lambda match, metadata, bot: True,
                 exec_condition=lambda match, metadata, bot: True, flags=None):
        self.regex = regex
        self.title = title
        self.description = description
        self.on_exec = self.run_on_sandbox
        self.func = on_exec
        self.display_condition = display_condition
        self.exec_condition = exec_condition
        self.flags = flags if flags is not None else []
        self.raw_match = raw_match

    def __str__(self):
        return '<Command "{}">'.format(self.title)

    def run_on_sandbox(self, match, metadata, bot):
        try:
            if self.exec_condition(match, metadata, bot):
                self.func(match, metadata, bot)
        except Exception as e:
            traceback.print_exc()
            bot.send("Oops, something went horribly wrong! (%s)" % e,
                     metadata['from_group'], metadata['_id'])


class CannedResponseCommand(Command):
    def __init__(self, regex, title, description, canned="", raw_match=False,
                 display_condition=lambda match, metadata, bot: True,
                 exec_condition=lambda match, metadata, bot: True, flags=None):
        super(CannedResponseCommand, self).__init__(regex, title, description,
                                                    self.canned, raw_match, display_condition, exec_condition, flags)
        self.canned_response = canned

    def canned(self, match, metadata, bot):
        bot.reply(self.canned_response,
                  metadata["message_id"], metadata['from_group'], metadata['_id'])
