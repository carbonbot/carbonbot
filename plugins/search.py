#!/usr/bin/env python3

import secrets
import requests
from siliconbot.command import Command
from siliconbot import string_util


searxes = ["https://search.modalogi.com/", "https://searx.openpandora.org/",   "https://search.paulla.asso.fr/",
           "https://search.seds.nl/",      "https://searx.win/",               "https://search.activemail.de/",
           "https://searx.site/",          "https://searx.at/",                "https://search.jpope.org/",
           "https://searx.aquilenet.fr/",  "https://searx.angristan.xyz/",     "https://s3arch.eu/",
           "https://searx.haxors.club/",   "https://searx.ru/",                "https://searx.pwoss.xyz/",
           "https://openworlds.info/",     "https://searx.zareldyn.net/",      "https://search.biboumail.fr/",
           "https://finden.tk/",           "https://searx.abenthung.it/",      "https://searx.nogafa.org/",
           "https://searx.dk/",            "https://searx.gotrust.de/",        "https://search.gibberfish.org/",
           "https://searx.prvcy.eu/",      "https://suche.elaon.de/",          "https://suche.ftp.sh/",
           "https://searx.cc/",            "https://searx.ch/",                "https://searx.nulltime.net/",
           "https://searx.me/",            "https://www.searx.de/",            "https://searx.laquadrature.net/",
           "https://searx.oe5tpo.com/",    "https://search.homecomputing.fr/", "https://searx.potato.hu/",
           "https://framabee.org/",        "https://searx.libmail.eu/",        "https://search.blackit.de/",
           "https://search.koehn.com/",    "https://searx.drakonix.net/",      "https://search.4ray.co/",
           "https://search.azkware.net/",  "https://search.alecpap.com/",      "https://search.st8.at/",
           "https://search.paviro.de/",    "https://searx.infini.fr/",         "https://searx.kvch.me/",
           "https://searx.fr32k.de/",      "https://searx.remote-shell.net/",  "https://wtf.roflcopter.fr/searx",
           "https://searchx.mobi/",        "https://searx.antirep.net/",       "https://search.stinpriza.org/",
           "https://search.mdosch.de/",    "https://www.searxs.eu/",           "https://searx.foo.li/",
           "https://searx.pofilo.fr/",     "https://searx.openhoofd.nl/",      "https://searx.ahh.si/",
           "https://searx.org/",           "https://search.spaeth.me/",        "https://p9e.de/",
           "https://s.bacafe.xyz/",        "https://searx.tyil.nl/",           "https://seeks.hsbp.org/",
           "https://icebal.com/",          "https://www.opengo.nl/",           "https://search.moravit.com/",
           "https://beezboo.com/",         "https://searx.itunix.eu/",         "https://search.snopyta.com/",
           "https://searx.solusar.de/"]


def request_searx(query, category, prev_servers=[]):
    server = secrets.choice(list(set(searxes)-set(prev_servers)))
    params = {'q': query, 'categories': category, 'safesearch': '2',
              'format': 'json', 'image_proxy': "True", 'language': 'en'}
    try:
        response = requests.get("{}?".format(
            server if server[-1] == '/' else server+"/"), params=params, headers={'User-agent': 'SiliconBot 2.0'})
        return server, response.json()
    except:
        print(server+" has not provided the results")
        if len(prev_servers) > 10:
            return None, None
        prev_servers.append(server)
        return request_searx(query, category, prev_servers)


def to_list(from_list, infix):
    string = ""
    size = len(from_list)
    for i in range(size):
        if i+1 is size and i > 0:
            string += infix+" "
        string += "'{}'".format(from_list[i])
        if i+1 is not size:
            string += ", "
    return string


def query(match, metadata, bot):
    if not match.group("query"):
        return
    reply = ""

    for _ in range(4):
        server, result_json = request_searx(match.group("query"), "general")
        if result_json and 'infoboxes' in result_json.keys() and len(result_json['infoboxes']):
            result = result_json['infoboxes'][0]

            if 'content' in result.keys() and result['content']:
                result['content'] = string_util.truncated_by_sentence(
                    result['content'])
                reply += '{content}\n'.format(**result)
            for link in result['urls']:
                reply += '{title}: {url} '.format(**link)
            break
        elif not server:
            reply = "The query has been rejected in too many servers; try again later."
            break

    if not reply:
        reply = "No results for '{}'".format(match.group("query"))

    bot.reply(reply, metadata["message_id"],
              metadata['from_group'], metadata['_id'])


def register_with(silicon):
    silicon.add_commands(
        # Instant Answers
        Command(r"\?(?: (?P<query>.+))?", "? <query>",
                "Search for Instant Answer in SearX instances", query),
    )
